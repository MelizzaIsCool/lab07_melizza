﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public enum CharacterState
{
    Idle = 0,
    WalkingForward = 1,
    WalkingBackwards = 2,
    RunningForward = 3,
    Jumping = 4,
    RunningBackwards = 5
};

public class Lab02b_PlayerControlPrediction : NetworkBehaviour
{
    //the moves the player is attempting but have not yet been acknowledged
    Queue<KeyCode> pendingMoves;

    //stores animation state
    CharacterState characterAnimationState;

    //public Rigidbody player;

    public Animator animatorController;

    public float playerSpeed;

    // Use this for initialization
    void Start()
    {
        InitState();
        predictedState = serverState;

        if (isLocalPlayer)
        {          
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }
        SyncState();
    }//end Start()

    struct PlayerState
    {
        public int movementNumber;
        public float positionX, positionY, positionZ;
        public float rotationX, rotationY, rotationZ;
        public CharacterState animationState;
    }

    //Will be the actual state of the player on the server
    [SyncVar(hook = "OnServerStateChange")]
    PlayerState serverState;

    //will be what the player is going to see at first
    //Client only
    PlayerState predictedState;

    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaPosX = 0, deltaPosY = 0, deltaPosZ = 0;
        float deltaRotationY = 0;

        switch (newKey)
        {
            case KeyCode.Q:
                deltaPosX = -playerSpeed;
                break;
            case KeyCode.S:
                deltaPosZ = -playerSpeed;
                break;
            case KeyCode.E:
                deltaPosX = playerSpeed;
                break;
            case KeyCode.W:
                deltaPosZ = playerSpeed;
                break;
            case KeyCode.A:
                deltaRotationY = -playerSpeed;
                break;
            case KeyCode.D:
                deltaRotationY = playerSpeed;
                break;
            case KeyCode.Space:
                deltaPosY = 0.5f;
                break;
        }
        return new PlayerState
        {
            movementNumber = 1 + previous.movementNumber,
            positionX = deltaPosX + previous.positionX,
            positionY = deltaPosY + previous.positionY,
            positionZ = deltaPosZ + previous.positionZ,
            rotationX = previous.rotationX,
            rotationY = deltaRotationY + previous.rotationY,
            rotationZ = previous.rotationZ,
            animationState = CalcAnimation(deltaPosX, deltaPosY, deltaPosZ, deltaRotationY)
    };
    }//end Move()

    CharacterState CalcAnimation(float deltaX, float deltaY, float deltaZ, float deltaRotationY)
    {
        if (deltaX == 0 && deltaY == 0 && deltaZ == 0)
        {
            return CharacterState.Idle;
        }
        if (deltaX != 0 || deltaZ != 0)
        {
            if (deltaX > 0 || deltaZ > 0)
            {
                if (playerSpeed > 1.0)
                    return CharacterState.RunningBackwards;
                else
                    return CharacterState.WalkingBackwards;
            }
            else
            {
                if (playerSpeed > 1.0)
                    return CharacterState.RunningForward;
                else
                    return CharacterState.WalkingForward;               
            }
        }
        return CharacterState.Idle;
    }

    [Server]
    void InitState()
    {
        serverState = new PlayerState
        {
            movementNumber = 0,
            positionX = -119f,
            positionY = 165.08f,
            positionZ = -924f,
            rotationX = 0f,
            rotationY = 0f,
            rotationZ = 0f
        };
    }//end InitState()

    void SyncState()
    {
        PlayerState stateToRenderer = 
                  isLocalPlayer ? predictedState : serverState;

        transform.position = new Vector3(
                  serverState.positionX, serverState.positionY, serverState.positionZ);

        transform.rotation = Quaternion.Euler(
                  serverState.rotationX, serverState.rotationY, serverState.rotationZ);

        animatorController.SetInteger("CharacterState", (int)stateToRenderer.animationState);
    }//end SyncState()

    void Update()
    {
        //[IMPORTAN]if using isLocalPlayer you gotta be using NetworkBehavior!
        //Gotta be an object with NetworkIdentity comp., and LocalPlayerAuthority
        //Don't run in Awake(), dont be an idiot, don't do it.
        if (isLocalPlayer)
        {
            //Debug.Log("Pending Moves: " + pendingMoves.Count);

            //an array to hold our possible keys to push
            KeyCode[] possibleKeys =
            {
                KeyCode.A,
                KeyCode.S,
                KeyCode.D,
                KeyCode.W,
                KeyCode.Q,
                KeyCode.E,
                KeyCode.Space
            };

            bool isAnyButtonPressed = false;

            foreach (KeyCode possibleKey in possibleKeys)
            {
                //if the key being observed is not being pushed...
                if (!Input.GetKey(possibleKey))
                {
                    //...then do nothing
                    continue;
                }

                isAnyButtonPressed = true;
                pendingMoves.Enqueue(possibleKey);
                UpdatePredictedState();
                CmdMoveOnServer(possibleKey);
            }

            if (!isAnyButtonPressed)
            {
                pendingMoves.Enqueue(KeyCode.Alpha0);
                UpdatePredictedState();
                CmdMoveOnServer(KeyCode.Alpha0);
            }
        }
        SyncState();
    }//end Update()

    void OnServerStateChange(PlayerState newSate)
    {
        serverState = newSate;

        if (pendingMoves != null)
        {
            while (pendingMoves.Count > 
                  (predictedState.movementNumber - serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }
            UpdatePredictedState();
        }
    }//end OnServerStateChange()

    void UpdatePredictedState()
    {
        predictedState = serverState;

        foreach (KeyCode moveKey in pendingMoves)
        {
            predictedState = Move(predictedState, moveKey);
        }
    }//end UpdatePredictedSate()

    [Command]//[IMPORTANT]Will only run if isLocalPlayer returns true
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = Move(serverState, pressedKey);
    }
}