﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Lab02a_PlayerControl : NetworkBehaviour
{
    // Use this for initialization
    void Start()
    {
        InitState();
        SyncState();
    }//end Start()

    struct PlayerState
    {
        public float positionX, positionY, positionZ;
        public float rotationX, rotationY, rotationZ;
    }

    [SyncVar]
    PlayerState state;

    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaPosX = 0, deltaPosY = 0, deltaPosZ = 0;
        float deltaRotationY = 0;

        switch (newKey)
        {
            case KeyCode.Q:
                deltaPosX = -0.5f;
                break;
            case KeyCode.S:
                deltaPosZ = -0.5f;
                break;
            case KeyCode.E:
                deltaPosX = 0.5f;
                break;
            case KeyCode.W:
                deltaPosZ = 0.5f;
                break;
            case KeyCode.A:
                deltaRotationY = -1.0f;
                break;
            case KeyCode.D:
                deltaRotationY = 1.0f;
                break;
        }
        return new PlayerState
        {
            positionX = deltaPosX + previous.positionX,
            positionY = deltaPosY + previous.positionY,
            positionZ = deltaPosZ + previous.positionZ,
            rotationX = previous.rotationX,
            rotationY = deltaRotationY + previous.rotationY,
            rotationZ = previous.rotationZ
        };
    }//end Move()

    [Server]
    void InitState()
    {
        state = new PlayerState
        {
            positionX = -119f,
            positionY = 165.08f,
            positionZ = -924f,
            rotationX = 0f,
            rotationY = 0f,
            rotationZ = 0f
        };
    }//end InitState()

    void SyncState()
    {
        transform.position = new Vector3(state.positionX, state.positionY, state.positionZ);
        transform.rotation = Quaternion.Euler(state.rotationX, state.rotationY, state.rotationZ);
    }//end SyncState()

    void Update()
    {
        //[IMPORTAN]if using isLocalPlayer you gotta be using NetworkBehavior!
        //Gotta be an object with NetworkIdentity comp., and LocalPlayerAuthority
        //Don't run in Awake(), dont be an idiot, don't do it.
        if (isLocalPlayer)
        {
            //an array to hold our possible keys to push
            KeyCode[] possibleKeys =
            {
                KeyCode.A,
                KeyCode.S,
                KeyCode.D,
                KeyCode.W,
                KeyCode.Q,
                KeyCode.E,
                KeyCode.Space
            };

            foreach (KeyCode possibleKey in possibleKeys)
            {
                //if the key being observed is not being pushed...
                if (!Input.GetKey(possibleKey))
                {
                    //...then do nothing
                    continue;
                }
                CmdMoveOnServer(possibleKey);
            }
        }
        SyncState();
    }//end Update()

    [Command]//[IMPORTANT]Will only run if isLocalPlayer returns true
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        state = Move(state, pressedKey);
    }
}
